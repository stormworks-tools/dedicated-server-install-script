@echo off
set mypath=%cd%

if exist "%mypath%\Stormworks Dedicated Server.pid" (

	set mypid = 0

	FOR /F "tokens=* USEBACKQ" %%F IN (`type "%mypath%\Stormworks Dedicated Server.pid"`) DO (
		taskkill /F /PID "%%F"
	)
	del "%mypath%\Stormworks Dedicated Server.pid"

	@echo on
	@echo Server is stopped
) else (
	@echo on
	@echo Server is not running
)