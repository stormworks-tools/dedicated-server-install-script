1. [Intro](#Intro)
2. [Install the server](#Install the server)
3. [Start the server](#Start the server)
4. [Stop the server](#Stop the server)
5. [Update the server](#Update the server)
6. [Uninstall the server](#Uninstall the server)


# Intro

These files can be used to automatically install a Stormworks Dedicated Server (**WINDOWS**) and then run it.
The files are a customized version of "condenser", which can be used for other servers, but i configurated it to only do the Stormworks Dedicated Server.

### Requirements
* you need to have access to the command line
* you need to have administrator access on the machine
* you need to be able to run powershell
* you need to adjust security settings to run the powershell script
* **if you don't want to to change the security settings or can't, you have to do the manual installation here**

### What does this script do?
* this will download a zip (including the custom version of condenser), unzip it, and move it to `C:\condenser`
* it will also install the official SteamCMD (which is the steam client, but without graphical interface) to `C:\steamcmd`
* it will change your powershell security settings to allow unsigned scripts

## Powershell Security Settings
Try to install it without changing these settings, if it throws an error (e.g. `BlaBlaBla execution is disabled on this device`)
THen change the setting:
* start powershell (hit windows button and search for powershell) don't run it as an administrator
* enter: `Set-ExecutionPolicy -Scope CurrentUser`
* now it will ask you which value you want to set it to
* enter `unrestricted`
* confirm (yes)
* leave window open (we will later revert this change)
* now run the install_server.bat again

If you ever want to revert this changes (e.g. you uninstalled the Stormworks Server) then follow these steps:
* enter: `Set-ExecutionPolicy -Scope CurrentUser`
* now it will ask you which value you want to set it to
* enter `undefined`
* confirm (yes)
* close the window

# Install

## Downloading the files

### Manually
https://gitlab.com/stormworks-tools/dedicated-server-install-script/-/archive/master/dedicated-server-install-script-master.zip

* Extract the above zip
* copy the content into `C:\condenser`
* The path to condenser.cmd should be `C:\condenser\condenser.cmd`

### Automatically via Command Line (curl must be installed!)
* open CMD
* `cd %HOMEPATH%\Downloads & curl -get "https://pastebin.com/raw/7aaeyCSY" > script.bat & script.bat & cd C:\condenser & @echo "" & @echo "Files successfully downloaded and unzipped!"`
* press Enter

## Download and install the server (and the neccessary official SteamCMD)
run the script `install_server.bat`
* if you see an error, go to the section "Powershell Security Settings"
* you will now see something like this:
```
[INFO] steamcmd not found at c:\steamcmd. Attempting to download...
[INFO] Downloaded and extracted steamcmd to c:\steamcmd

[INFO] steamcmd may prompt you for a Steam Guard code
Press any key to continue
```
* Now press a key (maybe you need to press it multiple times) until you see this:
```
[INFO] Updating Stormworks Dedicated Server...

[...]

[----] Downloading update (0 of 58,917 KB)...

[]...]
```
* when you see this:
```
[INFO] Done updating Stormworks Dedicated Server
```
* the server has been installed (or updated) successfully!

# Start the server
run the script `start_server.bat`

# Stop the server
run the script `stop_server.bat`

# Update the server
run the script `update_server.bat`

# Uninstall the server
### Desktop
* stop the server (see "Stop the server")
* delete the folder `C:\condenser`
* delete the folder `C:\steamcmd` (if you do not need SteamCMD anymore)

### Command Line
* stop the server (see "Stop the server")
* in the CMD:
* `rmdir /s C:\condenser`
* `rmdir /s C:\steamcmd` (if you do not need SteamCMD anymore)




