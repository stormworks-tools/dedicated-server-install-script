@echo off
set mypath=%cd%

if exist "%mypath%\Stormworks Dedicated Server.pid" (
	@echo on
	@echo Server is already running
) else (
	condenser -launch
	@echo on
	@echo Server is now running
)